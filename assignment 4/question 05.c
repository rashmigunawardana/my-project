#include <stdio.h>
int main (){
    int n, x, i,c;
    printf("Input a Number: ");
    scanf("%d", &n);
    printf("Maximum Number: ");
    scanf("%d", &x);
    for (i=1; i<=n; i++){
        printf("Multiplication Table for %d\n", i);
        for ( c = 1; c<= x; c++){
            printf("%d * %d = %d\n", i, c, i*c);
            }
        printf("\n");
        }
    return 0;
}

