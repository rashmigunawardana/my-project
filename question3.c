/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include<stdio.h>
int main() {
      double firstno, secondno, swap;
      printf("Enter first number: ");
      scanf("%lf", &firstno);
      printf("Enter second number: ");
      scanf("%lf", &secondno);
      swap = firstno;

      firstno = secondno;
      secondno = swap;

      printf("\nAfter swapping, firstNumber = %.2lf\n", firstno);
      printf("After swapping, secondNumber = %.2lf", secondno);
      return 0;
}
