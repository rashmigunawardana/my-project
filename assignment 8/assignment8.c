/*
    Rashmi Gunawardana
    Index no: 202040
*/
#include<stdio.h>
#include <string.h>

struct Student{
   char firstName[50];
   char subject[50];
   float marks;
};

int main(){
    int noOfStudents=0;
    /* Getting the number of student records */
    printf("Enter the number of Student Records: ");
    scanf("%d",&noOfStudents);

    struct Student records[noOfStudents];

    /* Getting student records */
    printf("\n---Enter Student Details---\n\n");
    for(int i=0; i<noOfStudents; i++){
        struct Student student0;
        printf("Enter student %d First Name: ",i+1);
        scanf("%s",&student0.firstName);
        printf("Enter Subject: ");
        scanf("%s",&student0.subject);
        printf("Enter Marks: ");
        scanf("%f",&student0.marks);
        records[i]=student0;
        printf("\n");
    }

    /* Printing student records */
    printf("\n---Displaying Student Information---\n\n");
    for(int i=0; i<noOfStudents; i++){
        printf("Student %d \n---------------\n",i+1);
        printf("First Name: %s \n",records[i].firstName);
        printf("Subject: %s \n",records[i].subject);
        printf("Marks: %.1f \n\n",records[i].marks);
    }

    return 0;
}
