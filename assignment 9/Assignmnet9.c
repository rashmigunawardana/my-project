/*
    Rashmi Gunawardana
    Index no: 20020432
*/

	#include<stdio.h>
	int main()
	{
	int c ;
	 char file[30];
	 FILE* fw,*fr,*fa;
	 //write
     char str[] = "UCSC is one of the leading institutes in Sri Lanka for computing studies.\n";
	 fw=fopen("Assignment9.txt","w");
	 fprintf(fw,str);
	 fclose(fw);
	 //read
	printf("Enter file name :");
	gets(file);
	fr=fopen(file,"r");
	if(fr==NULL)
	{
		printf("cannot open the file %s",file);
		exit(0);
	}
	while((c=getc(fr)) !=EOF)
	{
		putchar(c);
	}
	 //append
	fa=fopen("Assignment9.txt","a");
	fprintf(fa,"UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
	fclose(fa);
	 return 0 ;
	}
