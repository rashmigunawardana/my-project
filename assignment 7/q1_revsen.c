//Rashmi Gunawardana
//index no - 202040
//Write a C program to reverse a sentence entered by user.

#include <stdio.h>
int revsen();
int main() {
    printf("Enter a sentence: ");
    revsen();
    return 0;
}

int revsen() {
    char n;
    scanf("%c", &n);
    if (n != '\n') {
        revsen();
        printf("%c", n);
    }
    return 0;
}
