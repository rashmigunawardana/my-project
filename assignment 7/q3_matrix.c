/*
    rashmi gunawardana
    Index no: 202040

    Program to add and multiply two given matrices
*/
#include <stdio.h>

/* Function to add values for matrices */
void addValuesForMat(int rows, int colms, int mat[rows][colms]);
/* Function to print out the matrices */
void printValuesOfMat(int rows, int colms, int mat[rows][colms]);
/* Function for the addition of 2 matrices */
void addMat(int rows, int colms, int mat1[rows][colms], int mat2[rows][colms], int mat3[rows][colms]);
/* Function for the multiplication of 2 matrices */
void multiMat(int mat1_R,int mat1_C, int mat2_C, int mat1[mat1_R][mat1_C], int mat2[][mat2_C], int mat3[mat1_R][mat2_C]);

void addValuesForMat(int rows, int colms, int mat[rows][colms]){
    for (int i = 0; i < rows; i++){
        for (int j = 0; j < colms; j++){
            printf("Enter integer for row[%d], column[%d] : ",i+1,j+1);
            scanf("%d",&mat[i][j]);
        }
    }
}

void printValuesOfMat(int rows, int colms, int mat[rows][colms]){
    for (int i = 0; i < rows; i++){
        for (int j = 0; j < colms; j++){
            printf("%d  ",mat[i][j]);
            if(i==rows-1 && j==colms-1)printf("\n");
        }
        printf("\n");
    }
}

void addMat(int rows, int colms, int mat1[rows][colms], int mat2[rows][colms], int mat3[rows][colms]){
    for (int i = 0; i < rows; i++){
        for (int j = 0; j < colms; j++){
           mat3[i][j]=mat1[i][j]+mat2[i][j];
        }
    }
}

void multiMat(int mat1_R, int mat1_C, int mat2_C, int mat1[mat1_R][mat1_C], int mat2[][mat2_C], int mat3[mat1_R][mat2_C]){
    /* making the array elements to 0 */
    for (int i = 0; i < mat1_R; i++){
        for (int j = 0; j < mat2_C; j++){
           mat3[i][j]=0;
        }
    }
    /* multiplication of the 2 matrices */
    for(int i = 0; i < mat1_R; i++){
		for(int j = 0; j < mat2_C; j++){
			for(int k=0; k<mat1_C; k++){
				mat3[i][j] += mat1[i][k] * mat2[k][j];
			}
		}
	}
}

int main(){
    int matA_R, matA_C, matB_R, matB_C;
    printf("Enter no. of rows and columns in Matrix A:");
    scanf("%d%d",&matA_R, &matA_C);
    printf("Enter no. of rows and columns in Matrix B:");
    scanf("%d%d",&matB_R, &matB_C);

    int matA[matA_R][matA_C], matB[matB_R][matB_C], matAdd[matA_R][matA_C], matMulti[matA_R][matB_C];

    if(((matA_R==matB_R) && (matA_C==matB_C))||(matA_C==matB_R)){
        printf("\nEnter elements of Matrix A\n");
        addValuesForMat(matA_R,matA_C,matA);
        printf("\nEnter elements of Matrix B\n");
        addValuesForMat(matB_R,matB_C,matB);

        printf("Matrix A: \n");
        printValuesOfMat(matA_R,matA_C,matA);

        printf("Matrix B: \n");
        printValuesOfMat(matB_R,matB_C,matB);

        /* Addition of the 2 matrices */
        if(matA_R==matB_R && matA_C==matB_C){
            printf("A + B : \n");
            addMat(matA_R,matA_C,matA,matB,matAdd);
            printValuesOfMat(matA_R,matA_C,matAdd);
        }else{
            printf("To add two matrices no. of columns of Matrix A should equals to no. of columns in Matrix B\n and no. of rows of Matrix A should equals to no. of rows in Matrix B\n\n");
        }

        /* multiplication of the 2 matrices */
        if(matA_C==matB_R){
            printf("A * B : \n");
            multiMat(matA_R,matA_C,matB_C,matA,matB,matMulti);
            printValuesOfMat(matA_R,matB_C,matMulti);
        }else{
            printf("To multiply two matrices no. of columns of Matrix A should equals to no. of rows in Matrix B\n");
        }
    }else printf("Invalid Inputs\n");

    return 0;
}
