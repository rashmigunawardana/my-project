/*
    rashmi gunawardana
    Index no: 202040
    program to find the Frequency of a given character in a given string
   
*/
#include <stdio.h>
int main() {
    char str[1000], ch;
    int count = 0;

    printf("Enter a character: "); //the string entered by the user is stored in str.
    fgets(str, sizeof(str), stdin);

    printf("Enter the sentence/content: "); //the user is asked to enter the character whose frequency is to be found. This is stored in variable ch.
    scanf("%c", &ch);
// for loop is used to iterate over characters of the string. In each iteration, if the character in the string is equal to the ch, count is increased by 1.
    for (int i = 0; str[i] != '\0'; ++i) {
        if (ch == str[i])
            ++count;
    }

    printf("Frequency of %c = %d", ch, count); //the frequency stored in the count variable is printed.
    return 0;
}
