#include <stdio.h>
#define performancecost 500
#define forone 3

int attendees(int price);
int revenue(int price);
int cost(int price);
int profit(int price);

int attendees(int price){
   return (120-(price-15)/5*20);
   }
int revenue(int price){
   return price*attendees(price);
   }
int cost(int price){
   return performancecost + attendees(price)*forone;
   }
int profit(int price){
   return revenue(price)-cost(price);
   }
int main(){
   
   printf("Profit from different ticket prices \n\n");
   printf("Ticket Price\t Attendees \t Income \n");
	for(int tP = 5 ; tP<50; tP+=5){
      printf("Rs.%d \t\t %d \t\t Rs.%d\n", tP, attendees(tP), profit(tP));
      }
   
    return 0;
}

